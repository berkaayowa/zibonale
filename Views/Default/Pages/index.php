
<section id="home" class="h-lg cover dark bg-black home">
    <div class="carousel inner-controls" data-single-item="true" data-navigation="true" data-pagination="true" data-autoplay="10000">
        <!-- Slide -->
        <div class="slide h-lg">
            <div class="bg-image"><img src="/Views/Default/Assets/Home/home_1.jpg" alt=""></div>
            <div class="container v-center">
                <div class="row">
                    <div class="col-lg-6 col-md-8">
                        <h1 class="font-secondary color-green-theme">Zibonel FM</h1>
                        <h5 class="mb-40 text-muted">That was the big thing when I was growing up, singing on the radio</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slide -->
        <div class="slide h-lg">
            <div class="bg-image"><img src="/Views/Default/Assets/Home/home_2.jpg" alt=""></div>
            <div class="container v-center">
                <div class="row">
                    <div class="col-lg-6 col-md-8">
                        <h1 class="font-secondary color-green-theme">Zibonel FM</h1>
                        <h5 class="mb-40 text-muted">We owe thanks to the people who have listened to me over the years, who tuned in on the radio</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="live-wrapper">

</div>
<script>
	$app.initList(); 
</script>

